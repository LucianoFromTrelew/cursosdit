import { shallowMount } from "@vue/test-utils";
import CourseModal from "@/components/CourseModal.vue";

describe("CourseModal.vue", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(CourseModal);
  });

  it("should open on show()", () => {
    wrapper.vm.show({});
    expect(wrapper.vm.dialog).toBe(true);
  });

  it("should close on button click", () => {
    const btn = wrapper.find("#close-btn");
    btn.trigger("click");
    expect(wrapper.vm.dialog).toBe(false);
  });
});
