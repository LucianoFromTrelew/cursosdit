import { mount } from "@vue/test-utils";
import uuidv4 from "uuid";
import CourseCard from "@/components/CourseCard.vue";

describe("CourseCard.vue", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(CourseCard, {
      propsData: {
        id: uuidv4(),
        title: "lorem",
        timetable: "lorem",
        imgSrc: "lorem",
        inscriptionLink: "lorem"
      }
    });
  });

  it("calls onClickMore when on 'Ver más' button click", () => {
    const stub = jest.fn();
    wrapper.setMethods({ onClickMore: stub });

    const btn = wrapper.find("#see-more-btn");
    btn.trigger("click");

    expect(wrapper.vm.onClickMore).toBeCalled();
  });

  it("emits course-click on 'Ver más' button click", () => {
    const btn = wrapper.find("#see-more-btn");
    btn.trigger("click");
    expect(wrapper.emitted("course-click")).toBeTruthy();
  });

  it("renders title correctly", () => {
    const title = wrapper.find("[data-test='courseTitle']");
    expect(title.element.innerHTML).toBe("lorem");
  });

  it("renders button as anchor", () => {
    const btn = wrapper.find("[data-test='inscriptionLink']");
    expect(btn.contains("a")).toBeTruthy();
  });
});
