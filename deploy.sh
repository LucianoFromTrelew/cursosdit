#!/bin/bash

DIST_FOLDER=dist/
DEPLOY_URL="https://cursosdit.surge.sh"

rm -rf $DIST_FOLDER && \
    npm run build && \
    cd $DIST_FOLDER && \
    surge --domain $DEPLOY_URL && \
    cd .. && \
    rm -rf $DIST_FOLDER
