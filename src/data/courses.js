import uuidv4 from "uuid";

const courses = [
  {
    id: uuidv4(),
    title: "Introducción a git",
    timetable: "De 11 a 13 horas",
    description: `<b>Git</b> es un sistema de control de versiones, lo que significa que permite llevar registro de los cambios que se realizaron a los archivos de un <b>repositorio</b>; permite ver las distintas versiones por las que pasó, deshacer cambios y volver a un punto anterior, entre otras funcionalidades. Junto con plataformas como <b>GitLab</b>, facilita el trabajo en grupo de varias personas en un mismo proyecto`,
    requirements: `<p>Para este curso es necesario llevar <b>computadora personal</b>. Conocimientos básicos de programación son <i>recomendables</i></p>`,
    links: [
      {
        title: "Qué es git",
        link: "https://codigofacilito.com/articulos/que-es-git"
      },
      {
        title: "Git explicado en 2 minutos",
        link:
          "https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&cad=rja&uact=8&ved=2ahUKEwi71s6ZsPLdAhVFFZAKHRIHCyMQwqsBMAN6BAgEEAQ&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D5sXcjllHphk&usg=AOvVaw3r959XPwq7hmAU1JTkjZOr"
      },
      {
        title: "Libro ProGit en español",
        link:
          "https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones"
      }
    ],
    speakers: [
      { name: "Matias Acosta", twitterAccount: "@Matias_12tw" },
      { name: "Pablo Toledo Margalef", twitterAccount: "@T_Papablo" },
      { name: "Luciano Serruya Aloisi", twitterAccount: "@LucianoSerruya" }
    ],
    inscriptionLink: "https://goo.gl/forms/qzdsCMDilSOX9Da32",
    imgSrc: "https://git-scm.com/images/logos/downloads/Git-Icon-1788C.png"
  },
  {
    id: uuidv4(),
    title: "PHP + Laravel",
    timetable: "De 14 a 16 horas",
    description: `<p><b>PHP</b> es un lenguaje de código abierto muy popular especialmente adecuado para el desarrollo web y que puede ser incrustado en HTML.</p>
    <p><b>Laravel</b> es uno de los frameworks de código abierto más fáciles de asimilar para PHP. Es simple, muy potente y tiene una interfaz elegante y divertida de usar</p>`,
    requirements: `<p>Para este curso es necesario llevar <b>computadora personal</b>. Conocimientos básicos de programación son <b>necesarios</b></p>`,
    links: [
      {
        title: "Libro de introducción a PHP",
        link:
          "https://datenpdf.com/downloadFile/the-joy-of-php-alan-forbes-html-element-php_pdf"
      },
      { title: "Sitio de Laracasts", link: "https://laracasts.com/" },
      { title: "Documentación de PHP", link: "http://php.net/docs.php" },
      {
        title: "Qué es Laravel",
        link: "https://www.arsys.es/blog/programacion/que-es-laravel/"
      },
      {
        title: "Estadísticas de PHP",
        link: "https://www.similartech.com/technologies/php"
      }
    ],
    speakers: [
      { name: "Lucas Krmpotic", twitterAccount: "@lucaskrmpotic" },
      { name: "Germán Bianchini", twitterAccount: "@Ke_Panic" }
    ],
    inscriptionLink: "https://goo.gl/forms/DeM9eYezll3LXtX43",
    imgSrc:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/LaravelLogo.png/1200px-LaravelLogo.png"
  }
];

export default courses;
