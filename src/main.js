import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';

import Vuetify from 'vuetify';
Vue.use(Vuetify);
import 'vuetify/dist/vuetify.min.css';

import * as VueGoogleMaps from 'vue2-google-maps';
import gmapApiKey from "@/gmapApiKey";

Vue.use(VueGoogleMaps, {
  load: {
    key: gmapApiKey,
    libraries: 'places'
  }
})


Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount('#app');
